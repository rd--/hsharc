GL_GIT=git@gitlab.com:rd--/hsharc.git
GL_HTTP=https://gitlab.com/rd--/hsharc.git

all:
	echo "hsharc"

mk-cmd:
	echo "hsharc - NIL"

clean:
	rm -Rf dist dist-newstyle *~

push-gl:
	git push $(GL_GIT)

pull-gl:
	git pull $(GL_HTTP)

push-tags:
	git push $(GL_GIT) --tags

update-rd:
	ssh rd@rohandrape.net "(cd sw/hsharc ; git pull $(GL_HTTP))"

push-all:
	make push-gl update-rd
