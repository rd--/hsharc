import Data.List {- base -}
import Sound.Analysis.Sharc {- hsharc -}
import Sound.Sc3 {- hsc3 -}

type R = Double

posc :: (R,R,R) -> Ugen
posc (f,a,p) = fSinOsc ar (constant f) (constant p) * constant a

sosc :: [(R,R,R)] -> Ugen
sosc s = sum (map posc s) * (1 / fromIntegral (length s))

hear :: [(R,R,R)] -> IO ()
hear s = audition (out 0 (sosc s))

spectra :: [Instrument] -> String -> Int -> [(R,R,R)]
spectra sh nm j =
  let Just i = find ((== nm) . instrument_id) sh
  in note_spectra (note_normalise (notes i !! j))

{-
Right sh <- read_sharc "/home/rohan/opt/src/gregsandell/sharc/xml/sharc.xml"
map instrument_id sh

hear (spectra sh "violin_martele" 0)
hear (spectra sh "violin_pizzicato" 0)
hear (spectra sh "viola_muted_vibrato" 0)
hear (spectra sh "cello_vibrato" 0)
hear (spectra sh "CB_muted" 0)
hear (spectra sh "altoflute_vibrato" 0)
hear (spectra sh "oboe" 0)
hear (spectra sh "Bb_clarinet" 0)
hear (spectra sh "Bach_trumpet" 0)
hear (spectra sh "French_horn" 0)
hear (spectra sh "alto_trombone" 0)
hear (spectra sh "bass_trombone" 0)
-}
