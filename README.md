hsharc
------

minimal [haskell](http://haskell.org/) bindings to the
[Sandell Harmonic Archive](http://www.timbre.ws/sharc/)

© [rohan drape](http://rohandrape.net/),
  2007-2023,
  [gpl](http://gnu.org/copyleft/)
